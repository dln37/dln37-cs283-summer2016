To compile: make build
to run: make run
This will compile all three files and run each 10 times. All times in seconds

Part 1 results:
Final count: 100000     Time: 0.013518 
Final count: 100000     Time: 0.015284 
Final count: 100000     Time: 0.014372 
Final count: 100000     Time: 0.016072 
Final count: 100000     Time: 0.013962 
Final count: 100000     Time: 0.014274 
Final count: 100000     Time: 0.013678 
Final count: 97674      Time: 0.015815 
Final count: 99000      Time: 0.016154 
Final count: 100000     Time: 0.017615
		           AVG: .0150744
Trying to put the function in a for loop that runs 10 times seemed to give more incorrect results. My guess being that because it can call it faster than I can execute it 10 times, there's less room for a thread to be finished when another one wants to be made. i.e. the threads are more likely to lock up the creation of the next one.

Part2 results: 
Final count: 100000     Time: 0.457993 
Final count: 100000     Time: 0.431514
Final count: 100000     Time: 0.352359 
Final count: 100000     Time: 0.317095 
Final count: 100000     Time: 0.489928 
Final count: 100000     Time: 0.428446 
Final count: 100000     Time: 0.336177 
Final count: 100000     Time: 0.361738 
Final count: 100000     Time: 0.333503 
Final count: 100000     Time: 0.314813
                                          AVG: .372215
My guess is this is just an issue of efficiency. You can lock inside the for loop but that takes so much longer because of how many times you are locking but you can also call the locks outside of them and get the same result in a shorter amoount of time.

Part3 results:
Final count: 100000     Time: 0.017820 
Final count: 100000     Time: 0.018898 
Final count: 100000     Time: 0.011211 
Final count: 100000     Time: 0.013989 
Final count: 100000     Time: 0.018475 
Final count: 100000     Time: 0.012220 
Final count: 100000     Time: 0.011991 
Final count: 100000     Time: 0.017339 
Final count: 100000     Time: 0.015149 
Final count: 100000     Time: 0.015494
		         AVG: .0152586
This test was always right and returned times close to the original test.Just like what I said for the last one, calling mux only once per thread would still give the right count but faster(as far as these tests go anyway).