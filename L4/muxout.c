#include <stdio.h>
#include <pthread.h>
#include <time.h>

//Most code adapted from slides
volatile unsigned int cnt;
pthread_mutex_t mux = PTHREAD_MUTEX_INITIALIZER;
void *count()
{
    int i;
    //Locking
    pthread_mutex_lock(&mux);

    for(i=0; i<1000; i++)
    {
        cnt++;
    }
    //Unlocking
    pthread_mutex_unlock(&mux);
    return NULL;
}
int main()
{
    cnt = 0;
    pthread_t tids[100];
    //Clock for timing
    clock_t  start = clock();
    for(int i = 0; i < 100; i++)
    {
        pthread_create(&(tids[i]), NULL, count, NULL);
    }
    for(int i = 0; i < 100; i++)
    {
        pthread_join(tids[i], NULL);
    }
    clock_t stop = clock();
    //Time difference
    double elapsed = (double)(stop - start) / CLOCKS_PER_SEC;
    
    
    printf("Final count: %d	Time: %f\n\n", cnt, elapsed);
   
    return 0;
}

