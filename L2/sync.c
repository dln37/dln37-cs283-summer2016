#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
//#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include "csapp.h"

char* dir1 = NULL;
char* dir2 = NULL;

//Copy a to b
static int display_info(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf) {
    //printf("%s\n", fpath);
    
    struct stat path_st;
    //Check if file
    if(S_ISREG(sb->st_mode))
    {
        int tmpfd;
        char* tmppath = (char*) malloc(128 * sizeof(char));
        char* ppath = (char*) malloc(128 * sizeof(char));
        char* bpath = (char*) malloc(128 * sizeof(char));
        char* orpath = (char*) malloc(128 * sizeof(char));
        bzero(bpath, 128);
        bzero(orpath, 128);
        strcpy(tmppath, fpath);
        
        
        ppath = strtok(tmppath, "/");
        strcat(orpath, ppath);
        strcat(orpath, "/");
        
        ppath = strtok(NULL, "/");
        strcat(orpath, ppath);
        strcat(bpath, dir2);
        while(ppath != NULL)
        {
            ppath = strtok(NULL, "/");
            
            if(ppath != NULL)
            {
                    
                    strcat(bpath, "/");
                    strcat(bpath, ppath);
                    
                    strcat(orpath, "/");
                    strcat(orpath, ppath);
                    stat(orpath, &path_st);
                    
                if(S_ISREG(path_st.st_mode))
                {
                    break;
                }
                    mkdir(bpath, 0700);
                
            }
        }
        
        
        //We need to create the file if it doesn't exist
        if ((tmpfd = open(bpath, O_RDONLY)) < 0)
        {
            
            int fd;
            int fd2;
            //printf("%s\n", orpath);
            //printf("%s\n", bpath);
            fd = open(orpath, O_RDONLY);
            fd2 = open(bpath,  O_RDWR|O_CREAT, 0775);
            
            int* n = (int*) malloc(sizeof(int));
            rio_t rio;
            char* buf = (char*) malloc(512 * sizeof(char));
            
            Rio_readinitb(&rio, fd);
            while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)
            {
                Rio_writen(fd2, buf, n);
            }
            close(fd);
            close(fd2);
            close(tmpfd);
            free(n);

        }
        
        //If it does exist we need to compare their time stamps
        else
        {
            int fd;
            int fd2;
            printf("%s\n", bpath);

            
            int* n = (int*) malloc(sizeof(int));
            rio_t rio;
            char* buf = (char*) malloc(512 * sizeof(char));
            
            //Create the stats
            struct stat astat;
            struct stat bstat;
            
            //Need the stats
            stat(orpath, &astat);
            stat(bpath, &bstat);
            printf("%s %s\n", orpath, bpath);
            //If a is newer then replace b

            if(difftime(astat.st_mtime, bstat.st_mtime) > 0)
            {
                printf("file 1 newer\n");
                fd = open(orpath, O_RDONLY);
                fd2 = open(bpath,  O_WRONLY);
                
                Rio_readinitb(&rio, fd);
                while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)
                {
                    Rio_writen(fd2, buf, n);
                }
            }
        
            else
            {
                printf("file2 newer\n");
                fd = open(bpath, O_RDONLY);
                fd2 = open(orpath,  O_WRONLY);
                //if b is bigger then write into b
                Rio_readinitb(&rio, fd);
                while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0)
                {
                    Rio_writen(fd2, buf, n);
                }
            }
        
            close(fd);
            close(fd2);
        }
        
        
        
    }
    return 0; /* To tell nftw() to continue */

}

//Delete from b that's not in a
static int removeXtra(const char *fpath, const struct stat *sb, int tflag, struct FTW *ftwbuf)
{
    if(S_ISREG(sb->st_mode))
    {
        int fd;
        

            char* tmppath = (char*) malloc(128 * sizeof(char));
            char* part = (char*) malloc(128 * sizeof(char));
            char* bpath = (char*) malloc(128 * sizeof(char));
            char* apath = (char*) malloc(128 * sizeof(char));
            bzero(bpath, 128);
            bzero(apath, 128);
            strcpy(tmppath, fpath);
        
        
            part = strtok(tmppath, "/");
            //strcat(apath, part);
            //strcat(apath, "/");
        
            part = strtok(NULL, "/");
            //strcat(apath, part);
            strcat(apath, dir1);
            //strcat(apath, part);
            printf("apath: %s\n", apath);
            while(part != NULL)
            {
                part = strtok(NULL, "/");
            
                if(part != NULL)
                {
                    
                    //strcat(bpath, "/");
                    //strcat(bpath, part);
                    
                    strcat(apath, "/");
                    strcat(apath, part);
                }
            }
           
            if((fd = open(apath, O_RDONLY)) < 0)
            {
                printf("Need to remove: %s\n", fpath);
                remove(fpath);
            }
        
        
    }
    return 0;
}


// http://linux.die.net/man/3/ftw

int main(int argc, char *argv[]) 
{
    int* n = (int*) malloc(sizeof(int));
    rio_t rio;
    char buf[MAXLINE];
    int fd;
    int fd2;
    dir2 = "./b";
    dir1 = "./a";

    int flags = 0;
    

    
    
    if (nftw(dir1, display_info, 20, flags) == -1) 
    {
        perror("nftw");
        exit(EXIT_FAILURE);

    }
    
    if (nftw(dir2, removeXtra, 20, flags) == -1) 
    {
        printf("laaaaaaaaaa\n");
        perror("nftw");
        exit(EXIT_FAILURE);

    }
 
    
    close(fd);
    close(fd2);
 



    exit(EXIT_SUCCESS);

}