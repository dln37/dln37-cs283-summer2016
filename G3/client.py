from multiprocessing.connection import Client
import pickle
import time
import itertools
import socket
import sys
import random
import math
import fractions

def coprime(n, m):
    random.seed()
    x = random.randint(m*2, 10000)
    while(fractions.gcd(x,n) != 1 or fractions.gcd(x, m) != 1 or isPrime(x)):
        x = random.randint(m*2, 10000)
    return x

def findprime(n):
    count = 0
    l = []
    for num in range(1, 1000):
        if all(num%i != 0 for i in range(2, num)):
            l.append(num)
    return l[n + 1]

def egcd(a, b):
    if a ==0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def invmod(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('mod inv does not exist')
    else:
        return x % m

def isPrime(n):
    if n == 2 or n == 3: return True
    if n < 2 or n%2 == 0: return False
    if n < 9: return True
    if n%3 == 0: return False
    r = int(n**0.5)
    f = 5
    while f <= r:
        if n%f == 0: return False
        if n%(f+2) == 0: return False
        f +=6
    return True

def keygen(i = 8, j = 13 ):
    a = findprime(i)
    b = findprime(j)
    c = a*b
    m = (a - 1) * (b - 1)
    e = coprime(c, m)
    d = invmod(e, m)
    return [c, e, d]

def encrpytMess(inp):#i and j will be params
    c, e, d = keygen()
    str = inp#"Hello"
    l = []
    for ch in str:
        #print(ch ** e % c)
        l.append(ch ** e % c)
    return [e, c, l]
#decrypt stuff
    #possi = list(itertools.combinations(primes(c), 2))
    #for x in possi:
    #    m = (x[0] - 1) * (x[1] - 1)
     #   if(fractions.gcd(e, m) == 1):
      #      #print(m)
       #     print(chr(l[0] ** invmod(e, m) % c))

    #for el in l:
     #   print(chr(el ** d % c))

#from http://stackoverflow.com/questions/16996217/prime-factorization-list
def primes(n):
    primfac = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            primfac.append(d)  # supposing you want multiple factors repeated
            n //= d
        d += 1
    if n > 1:
       primfac.append(n)
    return primfac

def keycrack(e=1337, c=65535):
    possi = list(itertools.combinations(primes(c), 2))
    for x in possi:
        m = (x[0] - 1) * (x[1] - 1)
        if(fractions.gcd(e, m) == 1):
            print(m)
            #(248 ** invmod(e, m) % c)







#sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serverAdd = ('localhost', 8000)
print(sys.stderr, 'connecting to %s port %s' % serverAdd)
#sock.connect(serverAdd)

sock = Client(('localhost', 8000))

try:

    # Send data
    message = b'Hip Hip Cooray!!'
    print(sys.stderr, 'sending "%s"' % message)
    mess = encrpytMess(message)
    sock.send(mess)
    #sock.close()


finally:

    print(sys.stderr, 'closing socket')
    sock.close()

