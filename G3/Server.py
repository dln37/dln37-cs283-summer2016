#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Dustin
#
# Created:     13/08/2016
# Copyright:   (c) Dustin 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------
from multiprocessing.connection import Listener
import socket
import sys
import pickle
import os

#from http://stackoverflow.com/questions/16996217/prime-factorization-list
#and other stackoverflow sites
def egcd(a, b):
    if a ==0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)

def invmod(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('mod inv does not exist')
    else:
        return x % m

def primes(n):
    primfac = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            primfac.append(d)  # supposing you want multiple factors repeated
            n //= d
        d += 1
    if n > 1:
       primfac.append(n)
    return primfac

def keycrack(e=1337, c=65535, l=[]):
    x = primes(c)
    m = (x[0] - 1) * (x[1] - 1)
    d = invmod(e, m)
    print("Cracked Private key: (" + str(d) + ", " + str(c) + ")")
    #possi = list(itertools.combinations(primes(c), 2))
    #for x in possi:
    #    m = (x[0] - 1) * (x[1] - 1)
    #   if(fractions.gcd(e, m) == 1):
    #        print(m)
     #       #(248 ** invmod(e, m) % c)
    for el in l:
        print(chr(el ** d % c))

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

serveAdd = ('localhost', 8000)
print(sys.stderr, 'starting up on %s port %s' % serveAdd)
#sock.bind(serveAdd)

#sock.listen(1)
sock = Listener(('localhost', 8000))
os.system('client.py')
while True:
    print(sys.stderr, 'waiting for a connection')
    #connection, client_address = sock.accept()

    connection = sock.accept()
    try:
        #print(sys.stderr, 'connection from', client_address)
        while True:
            data = connection.recv()
            #print(pickle.loads(data))
            #print(get_msg(connection))
            #encMess = pickle.loads(data)
            print("Public key: (" + str(data[0]) + ", " + str(data[1]) + ")")
            print("Encrypted message:")
            for el in data[2]:
                print(str(el))
            #for x in data:
                #print(sys.stderr, 'recieved %s' % data)
            if data:
                keycrack(data[0], data[1], data[2])
                print(sys.stderr, 'sending data back to the client')
                #connection.send('asdfasdfasdf')
            else:
                #print(sys.stderr, 'no more data from', client_address)
                break
    except EOFError:
        print('blaahahah')
        #connection.close()
    finally:
        connection.close()

