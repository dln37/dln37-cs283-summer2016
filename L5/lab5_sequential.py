import sys
import datetime

filename = sys.argv[1]
header = ''

csvlines = []
csvfile = open(filename, 'r')
lineno = 0

for line in csvfile:
	if lineno > 0: 
		csvlines.append(line)
	else:
		header = line
	lineno = lineno + 1
result = dict()

for line in csvlines:
	lineno = lineno + 1
	data = line.split(',')
	key = tuple(sorted((data[4], data[7])))
	if not (key in result.keys()):
		result[key] = []
	stamp0 = data[11]
	stamp1 = data[12]
	expected = datetime.datetime.strptime(stamp0, '%Y-%m-%d %H:%M:%S')
	actual = datetime.datetime.strptime(stamp1, '%Y-%m-%d %H:%M:%S')
	delta = actual - expected
	val = (float(delta.seconds)/60) 
		
	result[key].append(val)
data_list = []
for r in result.keys():
	trainLine = r
	vals = result[r]
	totalcount = 0
	lessthan5 = 0
	for row in vals:
        	if row < 5.0:
                	lessthan5 = lessthan5 + 1
                totalcount = totalcount + 1
	ontpercent = float(lessthan5) / float(totalcount) * 100
	data = (trainLine, ontpercent)
	data_list.append(data)


for l in data_list:
           print "In", l[0], "Percentage on time within 5 minutes ", str(l[1]),'%'


