import csv
import datetime
def mapper(processed_data):	    
	result = dict()
	for line in processed_data:
		data = line.split(',')
		key = tuple(sorted((data[4], data[7])))

		if not (key in result.keys()):
			result[key] = []
		stamp0 = data[11]
		stamp1 = data[12]
		expected = datetime.datetime.strptime(stamp0, '%Y-%m-%d %H:%M:%S')
		actual = datetime.datetime.strptime(stamp1, '%Y-%m-%d %H:%M:%S')
		delta = actual - expected
		val = (float(delta.seconds)/60)
		result[key].append(val)
	return result

def reducer(tuple_list):

	trainLine = tuple_list[0]	
	vals = tuple_list[1]

	totalcount = 0
	lessthan5 = 0
	for row in vals:
		if row < 5:
			lessthan5 = lessthan5 + 1
		totalcount = totalcount + 1
	ontpercent = (float(lessthan5) / float(totalcount)) * 100
	data = (trainLine, ontpercent)
	return data

def main(): 
	data = dict()
	tuple_list = ('key', [5, 4, 2, 345, 543, 3, 2,4])
	print(reducer(tuple_list))

if __name__ == "__main__": 
	main()
