def partition(mappings_list):    
    # mappings is an array of dicts of train line keys to delay values, 
    # which we want to aggregate into a single dict organized by key, 
    # so that each reducer will receive only values from the same key    

    result = dict()           
    for mappings in mappings_list:
	for key in mappings.keys():            
        	for val in mappings[key]:                
            		if not (key in result):
                		result[key] = []
			result[key].append(val)     
    return result

