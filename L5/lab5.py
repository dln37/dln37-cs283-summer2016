import sys
from multiprocessing import Pool
from math import floor
import numpy as np
from mapper import mapper
from mapper import reducer
from partition import partition

filename = sys.argv[1]
processes = int(sys.argv[2])
header = ''

csvlines = []
csvfile = open(filename, 'r')
lineno = 0

for line in csvfile:
	if lineno > 0: 
		csvlines.append(line)
	else:
		header = line
	lineno = lineno + 1
#print(csvlines[1:10])
numlines = len(csvlines)
lines_per_process = int(floor(numlines/processes))

process_data_array = []
for i in range(processes):    
    starting = i * lines_per_process 
    ending = (i+1) * lines_per_process
    if(i == (processes - 1)): #to make sure no lines get ignored
        process_data_array.append(csvlines[starting:-1])
    else: 
	process_data_array.append(csvlines[starting:ending])
pool = Pool(processes=processes,)
mapper(process_data_array[0])
mapping = pool.map(mapper, process_data_array)    
shuffled = partition(mapping) # shuffled is an array of mapping dicts    
reduced = pool.map(reducer, shuffled.items()) # items is a list of (key, value) tuples
# Print the resulting dict of values aggregated from the reducers
for l in reduced:        
            print "In", l[0], "Percentage on time within 5 minutes ", str(l[1]),'%'
